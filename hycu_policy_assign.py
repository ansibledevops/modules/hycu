#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: hycu_policy_assign

short_description: Module to handle assigning and unassigning policies for HYCU Backups

version_added: "2.4"

description:
    - "Fill in later"

options:
    name:
        description:
            - This is the message to send to the sample module
        required: true
    new:
        description:
            - Control to demo if the result of this module is changed or not
        required: false

author:
    - Martin Fedec (@mfedec)
'''

EXAMPLES = '''
- name: Assign Policy to a VM
  hycu_policy_assign:
    server: hycu_server
    api_username: api_user
    api_password: password
    name: 
    policy_name:
    state: present

'''

RETURN = '''
NEED TO DO THIS.
original_message:
    description: The original name param that was passed in
    type: str
message:
    description: The output message that the sample module generates
'''

from ansible.module_utils.basic import AnsibleModule
import requests
import base64

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        server=dict(type='str', required=True),
        api_username=dict(type='str', required=True),
        api_password=dict(type='str', required=True, no_log=True),
        name=dict(type='str', required=True),  # This is VM, APP OR SHARE NAME
        type=dict(type='str', default='vm', choices=['vm', 'app', 'share']),  # Only working on VM atm.
        policy_name=dict(type='str', required=True),
        state=dict(type='str', default='present', choices=['present', 'absent'])
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message='',
        error='',
        data=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    api_url = "https://" + module.params['server'] + ":8443/rest/v1.0"
    changed = False

    # Base encode username and password to pass in with header.
    auth_details = module.params['api_username'] + ":" + module.params['api_password']
    encode_auth = base64.b64encode(auth_details.encode())

    headers = {
        "content-type": "application/json",
        "Authorization": "Basic " + encode_auth.decode()
    }

    # Start VM/App/Share Find
    vm_uuid = ""
    vm_protection_group = ""
    vm_protection_group_uuid = ""

    vm_params = {"filter": "vmName##" + module.params['name']}

    try:
        vm_get_response = requests.get(api_url + "/vms", params=vm_params, headers=headers, verify=False)
        vm_get_response.raise_for_status()
        vm_get_response = vm_get_response.json()
    except requests.exceptions.ConnectionError as e:
        result['error'] = 'Issue connecting to server'
        module.fail_json(msg='Issue connecting to backup server, please check your url', **result)
    except requests.exceptions.HTTPError as he:
        result['error'] = 'Issue with username or password'
        module.fail_json(msg='Please check your username and password', **result)

    if vm_get_response['metadata']['totalEntityCount'] == 1:
        vm_uuid = vm_get_response['entities'][0]['uuid']
        vm_protection_group_uuid = vm_get_response['entities'][0]['protectionGroupUuid']
        vm_protection_group = vm_get_response['entities'][0]['protectionGroupName']

        # print(vm_uuid)
        # print(vm_protection_group)
        # print(vm_protection_group_uuid)
    else:
        result['error'] = vm_get_response
        module.fail_json(msg='Unable to find vm ' + module.params['name'], **result)
    # End VM/App/Share Find

    # Get the Policy UUID from the supplied name.

    policy_params = {"filter": "name##" + module.params['policy_name']}

    policy_get_response = requests.get(api_url + "/policies", params=policy_params, headers=headers, verify=False)
    policy_get_response = policy_get_response.json()

    if policy_get_response['metadata']['totalEntityCount'] == 1:
        # Will grab the first item in the list, so best to give exact name of policy
        policy_uuid = policy_get_response['entities'][0]['uuid']
    else:
        result['error'] = policy_get_response
        module.fail_json(msg='Unable to find policy name ' + module.params['policy_name'], **result)

    # End Policy Find

    if module.params['state'] == 'present':
        if vm_protection_group_uuid != policy_uuid:
            changed = True
        print(changed)
    elif module.params['state'] == 'absent':
        if vm_protection_group_uuid == policy_uuid:
            changed = True

    if changed and not module.check_mode:
        policy_details = {
            "vmUuidList": [vm_uuid]
        }

        if module.params['state'] == 'present':
            # Set the policy
            policy_assign_response = requests.post(api_url + "/policies/" + policy_uuid + "/assign", json=policy_details, headers=headers, verify=False)
            if policy_assign_response.status_code == 200:
                result['message'] = "Policy has been successfully assigned"
                result['data'] = policy_assign_response.json()
            else:
                result['error'] = policy_assign_response.json()
                module.fail_json(msg='Failed to assign policy', **result)
        elif module.params['state'] == 'absent':
            # Remove the policy
            policy_unassign_response = requests.post(api_url + "/policies/unassign", json=policy_details, headers=headers, verify=False)
            if policy_unassign_response.status_code == 200:
                result['message'] = "Policy has been successfully unassigned"
                result['data'] = policy_unassign_response.json()
            else:
                result['error'] = policy_unassign_response.json()
                module.fail_json(msg='Failed to unassign policy', **result)

    result['changed'] = changed
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()